#!/usr/bin/env python3
"""
Alertmanager status applet using appindicator.

Install dependencies (`libffi7` is needed by `pygobject`):

  sudo apt install gir1.2-appindicator3-0.1 libffi7
"""

import os
from pathlib import Path
import time
from threading import Thread

import plac
import gi
import yaml
from query_alertmanager import get_alerts
from gi.repository import Gtk, AppIndicator3, Glib  # type: ignore
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')


# pylint: disable=too-many-instance-attributes
class Indicator():
    """Main class."""

    @plac.opt('config', "Config file location", type=Path)
    def __init__(self, config='~/.config/indicator_alertmanager/config.yml'):
        """Initialize idicator."""

        # Load config
        with open(Path.expanduser(config), encoding="UTF8") as config_file:  # type:ignore
            opts = yaml.load(config_file, Loader=yaml.FullLoader)
            self.url = opts.get('url')
            self.port = opts.get('port', 443)
            self.user = opts.get('user', 'admin')
            self.password = opts.get('password')
            self.icon_red = os.path.abspath("./prometheus.svg")
            self.icon_green = os.path.abspath("./prometheus-green.svg")

            self.app = 'alertmanager'
            self.indicator = AppIndicator3.Indicator.new(
                self.app, self.icon_green,
                AppIndicator3.IndicatorCategory.OTHER)
            self.indicator.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
            self.indicator.set_menu(self.create_menu())
            self.indicator.set_label("Checking…", self.app)
            # the thread:
            self.update = Thread(target=self.show_alerts)
            # daemonize the thread to make the indicator stopable
            # pylint: disable=deprecated-method
            self.update.setDaemon(True)
            self.update.start()

    def create_menu(self):
        """Create indicator menu."""
        menu = Gtk.Menu()

        item_details = Gtk.MenuItem(label='Open alertmanager URL')
        item_details.connect('activate', self.alert_details)
        menu.append(item_details)

        # separator
        menu_sep = Gtk.SeparatorMenuItem()
        menu.append(menu_sep)

        # quit
        item_quit = Gtk.MenuItem(label='Quit')
        item_quit.connect('activate', self.stop)
        menu.append(item_quit)

        menu.show_all()
        return menu

    def show_alerts(self):
        """Show alerts."""
        while True:
            alerts = get_alerts(url=self.url, port=self.port, user=self.user,
                                password=self.password)
            # print(alerts)
            # apply the interface update
            GLib.idle_add(
                self.indicator.set_label,
                alerts['summary'], self.app,
                priority=GLib.PRIORITY_DEFAULT)
            if alerts['count'] > 0:
                icon = self.icon_red
            else:
                icon = self.icon_green

            GLib.idle_add(self.indicator.set_icon, icon)
            time.sleep(30)

    def alert_details(self):
        """Open browser with alertmanager URL."""
        # alerts = get_alerts()
        # alert_count = len(alerts)
        # summary = f"{alert_count} alerts"
        # details = textwrap.fill(json.dumps(alerts), width=30)
        # alerts_item = Gtk.MenuItem(label=details)
        # self.menu.append(alerts_item)
        os.system(f"xdg-open {self.url}:{self.port}")

    def stop(self):
        """Exit indicator."""
        Gtk.main_quit()
