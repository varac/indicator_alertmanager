# Alertmanager status

`./alertmanager_status.py` will output the [alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/)
status details in a [waybar](https://github.com/Alexays/Waybar) compatible
JSON format.

It also holds a historic `indicator.py` python class for a tray icon
based on [libappindicator](https://launchpad.net/libappindicator). I don't use
it anymore and it doesn't work in the current state, although it might be
easy to get it working again.

## Todo

* Reduce duplicator code in `query_alertmanager.py` and `alertmanager_status.py`

## Install

Install dependencies:

```console
    pip install -r requirements.txt
```

## Standalone usage with command line arguments

```bash
./query_alertmanager.py https://alertmanager.example.net my_password
```

## Standalone usage config file

### Configuration

Config file example (`~/.config/alertmanager_status/config.yml`):

```yaml
---
url: https://alertmanager.example.com
port: 443
user: admin
password: 123
```

### Usage

```bash
./query_alertmanager.py https://alertmanager.example.net my_password
```

## Waybar integration

Additional requirement if you want waybar to parse the `on-right-click` URL
for the config file: [yq](https://github.com/mikefarah/yq)

```json
"custom/alertmanager": {
  "return-type": "json",
  "interval": 60,
  "format": "Alerts: {} {icon}",
  //"exec-if": "exit 0",                  // always run; consider advanced run conditions
  "format-icons": {
      "firing": "🔥",
      "snafu": "🟢",
      "error": "🚫"
  },
  "exec": "/bin/bash -c 'cd /tmp/alertmanager_status/ && . ./.direnv/*/bin/activate && ./alertmanager_status.py'",
  "signal": 21,
  "on-click": "pkill -RTMIN+21 waybar",
  "on-click-right": "xdg-open $(yq .url < ~/.config/alertmanager_status/config.yml)",
}
```

## Similar projects

* [Nagstamon](https://nagstamon.ifw-dresden.de/):
  > Nagstamon is a status monitor for the desktop. It connects to multiple
  > Nagios, Icinga, Opsview, Centreon, Op5 Monitor/Ninja, Checkmk and Thruk
  > monitoring servers

  Has also experimental `alertmanager` support, but [crashes on newer QT
  versions](https://github.com/HenriWahl/Nagstamon/issues/681)
